import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_FAILURE = 'CREATE_MESSAGE_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = data => ({type: FETCH_MESSAGES_SUCCESS, payload:data});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const createMessageRequest = () => ({type: CREATE_MESSAGE_REQUEST});
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});
export const createMessageFailure = () => ({type: CREATE_MESSAGE_FAILURE});

export const fetchMessages = () => {
  return async dispatch => {
    dispatch(fetchMessagesRequest());
    try {
      const response = await axios.get('http://localhost:8000/messages');
      dispatch(fetchMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchMessagesFailure());
    }
  };
};

export const createMessage = data => {
  return async dispatch => {
    dispatch(createMessageRequest());
    try {
      await axios.post('http://localhost:8000/messages', data);
      dispatch(createMessageSuccess());
    } catch (e) {
      dispatch(createMessageFailure());
      throw e;
    }
  };
};