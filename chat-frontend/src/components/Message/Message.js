import React from 'react';
import './Message.css'

const Message = ({author, message,datetime}) => {
    return (
        <div className='message'>
            <h1>{author}</h1>
            <p>{message}</p>
            <p>{datetime}</p>
        </div>
    );
};

export default Message;