import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/messagesActions";

const NewMessage = () => {
    const [message, setMessage] = useState({author: '', message: ''});

    const dispatch = useDispatch();

    const onInputChange = e => {
        const {name, value} = e.target;
        setMessage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const sendMessage = async data => {
        await dispatch(createMessage(data));
    };

    return (
        <div>
            <input
                placeholder='Author'
                type='text'
                name='author'
                value={message.author}
                onChange={onInputChange}/>
            <p></p>
            <input
                placeholder='Message'
                type='text'
                name='message'
                value={message.message}
                onChange={onInputChange}/>
            <p></p>
            <button onClick={()=>sendMessage(message)}>Send</button>
        </div>
    );
};

export default NewMessage;