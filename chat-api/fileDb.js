const fs = require('fs');
const {nanoid} = require('nanoid');

const filename = './db.json';
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    if(data.length < 30){
      return data;
    } else {
      const result = data.slice(-30);
      return result;
    }
  },

  addItem(item) {
    item.datetime = (new Date()).toISOString();
    item.id = nanoid();
    data.push(item);
    this.save();
    return item;
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data));
  }
};