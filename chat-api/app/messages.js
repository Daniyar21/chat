const express = require('express');
const path = require('path');
const {nanoid} = require('nanoid');
const fileDb = require('../fileDb');


const router = express.Router();

router.get('/', (req, res) => {
  const messages = fileDb.getItems();
  res.send(messages);
});


router.post('/', (req, res) => {
  if (!req.body.message || !req.body.author) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const message = {
    message: req.body.message,
    author: req.body.author,
  };

  const newProduct = fileDb.addItem(message);

  res.send(newProduct);
});

module.exports = router; // export default router;